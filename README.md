Mobian installer
================

This is an experimental installer from Mobian.

Features
========

* [x] Use Flash-Friendly File System (F2FS).
* [x] Self-contained installer. Can run from Mobian, UBports or other Linux OSes
* [x] Full installation takes 10 mins. Stream root/boot tarballs while installing.
* [x] Very simple and easy to debug
* [x] No need to compile it: it's just a Python script
* [ ] Tiny footprint
* [ ] Functional testable
* [ ] Check root/boot filesystem signatures or pin SSL certificates

Usage
=====

To install Mobian, see instructions on https://mobian.debian.net/

Developer info
==============

The `mobian_installer.py` script
==============================

The `mobian_installer.py` can be executed either manually or from the
self-extractable archive described above.

It creates the boot and root partitions, then downloads and installs
contents into these partitions.

The contents are downloaded from http://mobian.debian.net/
(currently without signature checking)

The `create_installer` script
===========================

The `create_installer` script is used for development on a Debian
computer. It generates the self-contained installer that is
published on http://mobian.debian.net/

On Debian and derivatives, it can be executed with
```
./create_installer
```
It does not require root privileges by itself, but requires that the
arm64 architecture is enabled, and the `makeself` package is
installed.
It generates a self-extractable `install_mobian.sh` executable
archive.

The archive must then be copied manually onto a mobile device, then
executed there.
On Debian and derivatives, it can be executed with
```
chmod +x install_mobian.sh
sudo ./install_mobian.sh
```
It extracts its contents to a temporary directory, then goes on
execution with its main script `mobian_installer.py`.

Bug reports
===========

Use https://salsa.debian.org/Mobian-team/mobian-installer/-/issues
